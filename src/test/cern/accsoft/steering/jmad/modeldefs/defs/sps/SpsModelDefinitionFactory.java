/**
 * 
 */
package cern.accsoft.steering.jmad.modeldefs.defs.sps;

import java.util.HashSet;
import java.util.Set;

import cern.accsoft.steering.jmad.domain.file.CallableModelFile.ParseType;
import cern.accsoft.steering.jmad.domain.file.CallableModelFileImpl;
import cern.accsoft.steering.jmad.domain.file.ModelFile.ModelFileLocation;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsets;
import cern.accsoft.steering.jmad.domain.file.ModelPathOffsetsImpl;
import cern.accsoft.steering.jmad.domain.machine.RangeDefinitionImpl;
import cern.accsoft.steering.jmad.domain.machine.SequenceDefinitionImpl;
import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;
import cern.accsoft.steering.jmad.modeldefs.ModelDefinitionFactory;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.JMadModelDefinitionImpl;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinition;
import cern.accsoft.steering.jmad.modeldefs.domain.OpticsDefinitionImpl;

/**
 * This class is the implementation of the model definition for the Sps
 * 
 * @author Kajetan Fuchsberger (kajetan.fuchsberger at cern.ch)
 * 
 */
public class SpsModelDefinitionFactory implements ModelDefinitionFactory {
    
private void addInitFiles(JMadModelDefinitionImpl modelDefinition) {
    /*
     * the length of a rectangular magnet is the distance between the
     * polefaces and not the arc length
     */
    modelDefinition.addInitFile(new CallableModelFileImpl("init-options.madx",
            ModelFileLocation.RESOURCE));

    /* beam */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "beams/lhc_beam_injection.beamx", ModelFileLocation.REPOSITORY));

    /* elements */
    modelDefinition.addInitFile(new CallableModelFileImpl("elements/sps.ele",ModelFileLocation.REPOSITORY));

    /*
     * call the aperture definition file for SPS. These are groups of
     * elements
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "aperture/aperturedb_1.dbx",ModelFileLocation.REPOSITORY));

    /*
     * aperture of QF1, QF2 and QD
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "aperture/aperturedb_2.dbx",ModelFileLocation.REPOSITORY));

    /*
     * call the sequence definition file for SPS
     */
    modelDefinition.addInitFile(new CallableModelFileImpl("sequence/sps.seq",ModelFileLocation.REPOSITORY));

    /*
     * aperture of individual elements
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "aperture/aperturedb_3.dbx",ModelFileLocation.REPOSITORY));

    /*
     * call the two strength definition files for SPS:.str & elements.str
     */
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/ft_noqs_ext.str",ModelFileLocation.REPOSITORY));
    
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/SPS_Q20.str",ModelFileLocation.REPOSITORY));
    
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/SPS_Q22.str",ModelFileLocation.REPOSITORY));
    
    modelDefinition.addInitFile(new CallableModelFileImpl(
            "strength/lhc_newwp.str",ModelFileLocation.REPOSITORY));
    
    modelDefinition.addInitFile(new CallableModelFileImpl("strength/elements.str",ModelFileLocation.REPOSITORY));
}

private ModelPathOffsets createModelPathOffsets() {
    ModelPathOffsetsImpl offsets = new ModelPathOffsetsImpl();
    offsets.setResourceOffset("sps");
    offsets.setRepositoryOffset("sps/2014");
    return offsets;
}

private Set<OpticsDefinition> createOpticsDefinitions(){
    Set<OpticsDefinition> definitionSet = new HashSet<>();
    definitionSet.add( new OpticsDefinitionImpl(
            "SPS-LHC-2014v1", new CallableModelFileImpl(
                    "strength/lhc_newwp.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    definitionSet.add(new OpticsDefinitionImpl(
            "SPS-FT-2014v1", new CallableModelFileImpl(
                    "strength/ft_noqs_ext.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    definitionSet.add(new OpticsDefinitionImpl(
            "SPS-Q20-2014v1", new CallableModelFileImpl(
                    "strength/SPS_Q20.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    
    definitionSet.add(new OpticsDefinitionImpl(
            "SPS-Q22-2016v1", new CallableModelFileImpl(
                    "strength/SPS_Q22.str",
                    ModelFileLocation.REPOSITORY, ParseType.STRENGTHS)));
    return definitionSet;
}

	@Override
	public JMadModelDefinition create() {
		JMadModelDefinitionImpl modelDefinition = new JMadModelDefinitionImpl();
		
		modelDefinition.setName("SPS");
		modelDefinition.setModelPathOffsets(createModelPathOffsets());

		this.addInitFiles(modelDefinition);
		
		for(OpticsDefinition opticsDefinition :  createOpticsDefinitions()) {
		modelDefinition.addOpticsDefinition(opticsDefinition);
		if(opticsDefinition.getName()=="SPS-LHC-2014v1") 
		    modelDefinition.setDefaultOpticsDefinition(opticsDefinition);
		}
		
		/*
		 * SEQUENCE
		 */
		SequenceDefinitionImpl sps = new SequenceDefinitionImpl("sps", null);
		modelDefinition.setDefaultSequenceDefinition(sps);

		TwissInitialConditionsImpl twiss = new TwissInitialConditionsImpl(
				"default-twiss");
		//twiss.setAlfx(alfx);
		twiss.setCalcAtCenter(true);
		twiss.setCalcChromaticFunctions(true);
		sps.setDefaultRangeDefinition(new RangeDefinitionImpl(sps, "ALL",
						twiss));

		return modelDefinition;
	}
}
